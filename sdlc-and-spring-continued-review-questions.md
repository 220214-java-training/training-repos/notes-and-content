## SDLC
- What are some common software development lifecycles?
- Describe the waterfall approach to software development.
- Describe the agile approach to software development.
- What are some benefits of agile over waterfall?
- What are some benefits of waterfall over agile?
- Are "agile" and "scrum" the same? If not, differentiate between the two.
- What is a sprint?
- What is a user story?
- What is story pointing?
- What is a burndown chart?
- What is velocity in the context of scrum?
- What is a product backlog? A sprint backlog?
- What are some of the regular scrum meetings that occur?

---

## Spring Review Questions Continued

- What is Spring Boot?
- What are some benefits of using Spring Boot over just incorporating Spring modules into your application?
- Where can you configure additional config in a spring boot app?
- What is needed to run a spring boot app?

---

- What is Spring MVC?
- Describe the flow of a request in Spring MVC.
- What are some of the Spring MVC annotations?
- What is the difference between @Controller and @RestController?
- What is the difference between @RequestMapping and @GetMapping?
- What is the difference between @RequestMapping on the class level vs the method level?
- What is the difference between using the @RequestParam annotation for a GET request vs a POST request?

---

- What is JPA?
- What is Hibernate?
- What is an ORM tool?
  - What are some benefits of using an ORM?
- What are some of the common JPA annotations?
- What is Spring Data JPA?
- How can you establish relationships between tables using JPA annotations?
- What is a JpaRepository?
- What is the @Query annotation used for?
- What is the @Transactional annotation used for?
- How can you perform server side validation using Spring Boot?

---

- What is reflection?
  - What are some of the ways reflection is used behind the scenes with Spring?