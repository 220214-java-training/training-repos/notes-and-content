## HTML Practice

1. Create an HTML page. Define the structure of the HTML web page.

    <details>
        <summary>Possible solution</summary>        
                <xmp>
                    <!DOCTYPE html>
                    <html lang="en">
                    <head>
                    </head>
                    <body>
                    </body>
                    </html> 
            </code>      
    </details>

2. Add a title to the web page that will be rendered in the browser's tab.

3. Add in a heading to the top of the web page.

4. Add a list with your top 3 favorite fruits. 
    - How could you make this a bulleted list? 
    - How would you make it a numbered list.

5. Add an image of your favorite fruit.

6. Set up a form for someone to subscribe to your newsletter. This requires their email, their birthday, their favorite color, and their favorite fruit.
    - When selecting the fruit, how would your HTML be different if you used a text input vs a select vs mulitselect vs datalist? How would they render differently?
    - What do you notice when you submit this form?