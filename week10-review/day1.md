# Git

A software tool to handle version control. Typically, for other software projects, but can be used for anything.

It's not as simple as everyone working together on one version at a time. Instead, you have everyone on the team working on different parts of the next version all in parallel.

Git allows developers to interact with their codebase along with an entire timeline of versions.
Each version is known as a "commit". The terminology is similar to that in SQL, where when a transaction is committed, it is finalized in the DB. When you make a commit in a git repository, a new version is finalized in the timeline.

On top of this, git allows ways to handle multiple "branches" of the timeline.

This is the core feature used to support multiple team members working in parallel on a project.

![Example Git Timeline](https://res.cloudinary.com/practicaldev/image/fetch/s--0feKXAUf--/c_imagga_scale,f_auto,fl_progressive,h_900,q_auto,w_1600/https://thepracticaldev.s3.amazonaws.com/i/48schr7iqawvz6skkkbb.png)

Overall, developers will typically make a new branch when making any new change to a git timeline. It is never recommended to directly interact with the main timeline. As if some change needs to be discarded it is much more difficult to manage if it was on the main timeline/branch.

Developers will create new branches to work on specific features. It is generally better practice to name the branches after the tasks that are being accomplished on these branches. It is not recommended to name branches after individuals.

We instead want very short-duration branches that are either deleted or merged in frequently. Once a branch is merged in, it is then deleted. The only long-term branches should be a main timeline/branch and possibly a dev branch/timeline. The dev branch might be used to consolidate all of the changes for a new version release, or similar.

We never want to re-use an old branch. We want our branches to be very focused in their purpose.

## Important Note

When multiple team members are working on different branches, and they each add their own tasks/work. They might end up modifying the same file in parallel.

If these changes are then merged together, a conflict arises. Git does not automatically know which change should take preference. It's possible that both changes should be discarded. Maybe both changes should be kept, or just one of the two. It is also possible, that neither change alone is sufficient, and more development needs to occur to combine them.

This is not something that Git can accomplish by itself. This _must_ be managed by a human. Specifically the person who is attempting to make the merge.

This is what is called a Merge Conflict. Merge Conflicts are _not_ inherently bad. They are inevitable. It is possible to reduce their frequency, by avoiding working on the same file in parallel, but this is not always possible.

## Making Small Commits

Specifically, in order to reduce the frequency of merge conflicts, we need to avoid working on the same file in parallel between any given merge.

One approach that is very helpful for this is to make very frequent and small merges.
If your branches/timelines only introduce a very small set of changes, the chances of a simultaneous branch making a similar change will therefore also be small. This only works if you also merge those changes frequently.

This will reduce the likelihood of merge conflicts, and even when they do occur, they will be small, because a few changes are being merged at any given time.

This is a workflow that does take practice and diligence to follow. It does become very easy for just a single member to be lazy and not push their code or merge their code while they work. But even a single member doing this, may result in a large merge conflict. But it isn't guaranteed.

Your team must be in constant communication about the tasks they are working on and the git flow they have been using. The rest of your team should be familiar with roughly how large/small the changes are on your branch (approx. how many commits), and the approximate timeline for when they will be complete.

### Git vs Communication

Often times, issues with communication will result in issues with Git. The easiest way to have a really smooth git workflow is to talk to your team members. Understand what they are working on, and what the scope of their changes are in the codebase. And work together to break all tasks up into small stages to merge in frequently.

## Git Rebase

Git includes a command known as a `git rebase`. This `rebase` command allows to perform some advanced interactions with our git timeline. Instead of simply making new commits, using a rebase, we can directly modify the timeline. We can change the past of the timeline. We can delete or merge commits. We can break commits apart. We can override timelines with a different history.

We can perform a lot of really powerful operations. However, generally speaking you should a bit careful when performing these operations, because they cannot be reversed.

Unlike a regular git workflow, where any change can always be tracked and reversed.

What a rebase does is it will shift a timeline to be after the events of another timeline.

![Git Rebase Animation](https://miro.medium.com/max/1400/0*HAd2rzmvSR90Gv3H.gif)

A Git rebase travels back in time to a point on a source timeline, and replays the commits on the destination timeline, one by one.

This is helpful when the main timeline introduces new commits that a branched timeline does not yet have. We can replay the commits from the branched timeline one by one on top of the new main timeline. By the end, our branched timeline now includes everything from the main timeline.

This is in contrast to a "Merge". A "Merge" will take all of the changes from a source timeline, combine them together into a single commit, and add that commit to the end of the destination timeline.

Because of this, Merges tend to result in large conflicts when you merge branches with a lot of new commits. Even if every single commit was very small, they are all combined into one and then merged. This could result in a large merge conflict.

This can still be avoided if you merge frequently enough when you only have a few new commits at a time.

The benefits are that you are never modifying the timeline itself, so you do have a full history of when these changes occurred.

### Interactive Rebase

While performing a git rebase, we can optionally perform some modifications to the timeline. This is known as an "interactive" rebase. This is what allows for large-scale changes to the timeline that can potentially end up non-reversible if followed up with overridding the main timeline.

While working with git, your local repository may sometimes be out of sync with the remote repository. Examples include when a different member of your team merges their code. Shortly after this occurs, the remote repository (GitLab or GitHub) contains the new changes, but your laptop/computer does not. This is an example of the local and remote repositories being out of sync.

To address this, we typically just pull in the new changes. However, when performing an interactive rebase, this is not the preferred solution.

If we just made changes to the timeline, of course our local timeline would not match. Our solution should necessarily be to re-include the other timeline, since we just went through the effort of changing it. Instead of pulling the remote timeline, we want to override the remote timeline with the new one that we just modified. This is accomplished by performing a force push: `git push --force` or `git push -f`.

When you force your changes, git will override the destination timeline with what you have locally. This should _never_, under any circumstances, occur on the main branch.

If you only force-push on a feature-branch, the main timeline is still unaffected. And if you need to revert, it's no longer the end of the world.

If you do make a force-push to override the timeline, as long as at least 1 person still has the original timeline on their local computers, it is possible for them to override it back. You can still recover in these cases.