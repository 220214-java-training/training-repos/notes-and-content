# Dependency Injection

Dependency Injection as a concept is simply trying to reduce code such as the following:

```java
public class MyService {
    private MyRepository myRepository = new MyRepository();

    public MyService() {
    }

    // Various other service methods below
}
```

into

```java
public class MyService {
    private MyRepository myRepository;

    public MyService() {
    }

    // Various other service methods below
}
```

We do not want to instantiate our dependencies directly.
This simple idea ends up being very powerful when you consider certain classes may need potentially dozens of dependencies.

Typically, this is accomplished using some 3rd party tool that is designed to invoke the constructors for us. In our case, we typically use Spring Framework for the backend, and Angular for the frontend.

Spring Framework accomplishes through very clever use of Java Reflection. Spring Framework uses Reflection to analyze your own application code, find any classes that support injection, discover the dependencies, instantiate those dependencies, and use them to instantiate your class.

In particular, Spring Framework introduces some new terminology to handle/manage these dependencies and which classes can or cannot be injected into.

Any class that supports dependency injection is known as a "Spring Bean". Any class that supports dependency injection is also instantiated/managed by Spring.

We would say that a "Spring Bean" is a class whose lifetime is managed by the Spring IoC Container.
What this means, is that this Spring IoC Container is responsible for instantiating (calling the constructor) the object and destroying the object.

IoC stands for "Inversion of Control". Inversion of Control is the concept of reversing the application flow to give control over some/certain aspects of the application to some part of the application. In our case, the control that we "inverting" is the control to construct/destroy objects, specifically Spring Beans.

Dependency Injection is one specific example/implementation of Inversion of Control. There are other forms of Inversion of Control, however Dependency Injection is the most well-known example.

We refer to the Spring IoC Container as a "container" because this is the object that tracks and holds onto, or "contains" all of our spring beans.

## Where the real benefits are

If all we could Spring Framework for was to instantiate our own objects for us, it wouldn't really be that beneficial outside of pre-existing large-scale enterprise applications.

Spring Framework goes a step beyond, and instantiates a bunch of objects that might be useful for a variety of Web Applications.

We've seen in a previous example, that Spring is capable of injecting an instance of an `HttpSession` object. We didn't write the `HttpSession` class, but Spring Framework will include that as a Spring Bean anyways. It's not just the `HttpSession` class.

# Servlets

When industry began developing web applications, Java wanted a way to provide a way to directly interact with the web and HTTP through its own API. So Java introduced the Servlet API. It is built within Java itself.

A Servlet is a Java class that is responsible for integrating with a web server to receive and process HTTP requests. Once processed, the Servlet hands the result back to the web server to be returned to the client.

Servlets included some overridden methods called `doPost`, `doGet`, etc that are invoked when a request for a given HTTP method is being processed.

The application developer (us) will implement these methods to process any incoming requests. At the time, it wasn't particularly easy to organize the implementations of requests to different URLs within a single servlet. You needed to manually process the incoming URL to determine what actions to take. This was one of the challenges that we wanted to simplify going forward.

The web server interacts with the servlet by invoking 3 specific methods: `init`, `service`, and `destroy`.

The order/timing upon which these methods are invoked are known as the `Servlet Lifecycle`. This is mostly because the first method involves instantiating and initializing the Servlet (the beginning of its lifetime). And the last method involves destroying the servlet (the end of its lifetime).

When requests initially come in, the web server finds which Servlet is responsible for processing the request and, if it is isn't initialized, the web server instantiates it and invokes the `init` method. From there, the web server will invoke the `service` method to process the request.

The Servlet will stay alive and initialized to receive more incoming requests. If a particular long time happens without any requests coming into that servlet, the web server will invoke the `destroy` method to save on memory. Note that the specific amount of time often depends on the specific web server you are using, so it isn't all that consistent. However, it is typically in the range of 24+ hours.

This `service` method has already been defined by a parent class to invoke the corresponding `doGet`, `doPost`, etc methods. So we do not have to specifically implement any of the 3 lifecycle methods.

The developer does also need to ensure the servlet is mapped to a url, so that the web server knows which urls the servlet is responsible for processing. If this configuration is not set, the web server will never attempt to initialize the servlet. And it will process no requests.

The way this is configured is through a "deployment descriptor". This is the `web.xml` file that you often find in these types of web applications.

The name, "Deployment Descriptor" really arises from the context on how the web server works.
The web server generally is hosted externally on some computer, and it understands how to deploy certain applications within it. Our applications must define or "describe" the ways in which the web server should deploy this specific application. Hence, "Deployment Descriptor".

In order for our web servers to deploy our java applications within themselves, the application must be packaged as a "Web Archive" (WAR) file. These WAR files are specifically designed to include certain files, such as the deployment descriptor, that inform the web server on what to do.

So if you attempt to provide a "Java Archive" (JAR) file to a web server, the web server will not be capable to deploying the application.

## Deployment

Over time, the Industry found challenges with managing many web servers. These web servers must be individually updated and maintained, but these individual updates might change the way applications deployed within them operate.

This required companies to closely monitor/track which applications are deployed to which web servers, and what versions those web servers and applications are. As some applications could be updated over time to function properly on an updated web server.

Handling all of this versioning was a bit of a nightmare. It was easy to accidentally make a mistake and have certain applications not function properly. Tracking down these bugs was an even worse nightmare.

So, the Industry wanted to find a better way. Over time, the idea of using an Embedded web server arose.

Instead of packaging our applications within a WAR file and deploying them to an external web server, we can have our java applications themselves create and start a small web server, and deploy their application within themselves.

This allows our applications to once again be packaged as a JAR file, as the application itself will handle the creation/configuration of any necessary deployment descriptors and other config.

Additionally, the versioning of the web servers can be handled by each application individually. So in order to deploy the resulting application, you simply needed the JRE and "run" the java application.

This does result in a potential performance loss, but over time the industry has largely optimized this so it isn't too much of a loss. This solves the versioning/deployment nightmare.

Even further, once Frameworks started being developed, such as Spring Framework, they followed these same patterns. So Spring Boot uses an Embedded Tomcat web server, and therefore our Spring Boot applications are packaged as a JAR, not as a WAR.