# DevOps

What is DevOps?

What exactly does "Operations" mean?

What does it mean to "scale" an application?

What are the differences between Horizontal and Vertical Scaling?

What does it mean to "deploy" an application?

What are the three DevOps Practices?

What is a DevOps "Pipeline"?

What is the difference between Continuous Delivery and Continuous Deployment?

What steps does a typical Continuous Delivery Pipeline include?

How did you deploy your frontend and backend applications in training? (Ran the Spring Boot JAR file on an EC2 and deployed frontend static assets to S3 bucket for static website hosting)

# Jenkins

What is Jenkins?

What is a Jenkins agent?

What is a Jenkins Job?

What steps must I follow to set up a Jenkins Pipeline?

# SonarCloud & Family

What is SonarLint?

What is a Linter?

What is a "Code Smell"?

What is SonarCloud?

What is SonarQube?

What are the differences between SonarCloud and SonarQube?

# AWS

What is AWS?

What common services does AWS offer that you used for creating a DevOps pipeline?

What is an EC2 key-pair? What can it be used for?

What is EC2? S3? RDS? AMI? IAM? (Elastic Compute Cloud, Simple Storage Service, Relational Database Service, Amazon Machine Image, Identity & Access Management)

What is the purpose for each of the above AWS services?

What is an AWS Security Group? In what ways can I configure them?

# Linux

What is Linux?

What are Operating Systems?

What are a few basic/common Linux commands? (man, cd, ls, pwd, mkdir, rm, mv, cat, touch, echo, grep, yum, ssh, scp, sudo, chmod)
What do they do?

What are some less common Linux commands? (usermod, chown, chgrp, which, systemctl)
What do they do?

What are Command Flags? Examples?

What are Command Line Editors? Examples?

What are Environment Variables?

What are Package Managers? What are they used for? Examples? (yum, apt-get, npm)