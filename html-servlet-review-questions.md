### HTML Review Questions
- What is HTML?
- What are some common HTML tags?
- Describe the structure of an HTML web page?
- What is the difference between an inline and block element in HTML?
  - Examples of each?
- How would I create a form with HTML?
  - What types of elements can I put in a form?
  - How could I submit a form?
- What some of the input types in HTML?
- How can I create a dropdown? How could I select multiple options?


### Servlet Review Questions

- What is a Web Server?
- Describe client/server architecture.
- What is Tomcat? What does it do for us?
- What is a Servlet?
- How can I create a Servlet?
- Describe the Servlet hierarchy.
- What are the lifecycle methods of a Servlet?
- What are the HTTP verbs?
- What are some common HTTP status codes?
- What is the deployment descriptor?
  - What is it's role in our app?
  - How can we configure it?
- What are the different handler methods in our Servlet?
  - What are these methods used for?
  - Describe an example method signature.
- What is the default implementation of the HttpServlet handler methods?
- Describe how you might use the HttpServletRequest object?
- Describe how you might use the HttpServletResponse object?
- Describe the workflow of an HTTP request and response being processed by our server.
- How can you process a form using a Servlet?
- What is the appropriate "Content-Type" header for a JSON payload?
- What is the appropriate "Content-Type" header for form input?
- What in your form determines the key value pairs sent to your server? Where in the HTTP request are those key value pairs going to be sent?
- What are parameters used for in a GET request?
- How are parameters different in a GET and POST request?