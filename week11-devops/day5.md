# Steps to set up a Jenkins Pipeline (Backend Example)

1. Spin up an AWS EC2
    - Choose AMI (Amazon Linux 2 is the one we used for training)
    - Choose Instance Type (t2.micro for the AWS Free Tier)
    - Choose Storage (Default 8 GB; Up to 30 GBs of storage allowed for AWS Free Tier)
    - Configure misc settings (Default settings is okay)
    - Choose SSH Key
        - Download if a new key is created
2. Install Dependencies
    - Java
    - Maven
    - Git
    - Jenkins
3. Install Suggested Plugins and setup Admin user
    - Must access the initialAdminPassword
4. Install any extra necessary plugins such as GitLab
5. Configure those plugins accordingly
    - Need to create a Personal Access Token on GitLab to give Jenkins access
6. Create Freestyle Project
    - Setup Source Code repository via Git and configure accordingly
    - Choose Build Trigger from GitLab
    - Setup GitLab webhook via Jenkins GitLab integration
7. Configure Build Steps
    - Choose `Execute Shell` step to run linux commands
    - Include commands to build, test, and deploy application
8. Configure Post-Build Step to publish build status to GitLab