# DevOps

DevOps is a blend between CS and IT. The name "DevOps" comes from combining "Development" and "Operations". We have an intiuitive sense of what Development means what it is, but not as much for Operations.

Operations has to do with deploying the application to production and maintaining it from thereon.
If the application crashes or is overloaded, it is up to the Operations team to resolve it.

Historically, these 2 teams have been separate. Development created the application and implemented new features, while a separate team would take the application, deploy it, and maintain it in the production environment.

We don't want to keep things separated in this way, as it resulted in a lot of friction.

Overall, the development team wants to implement features as quickly as possible to release the features to their users. To have an improved user experience. Additionally, as we discussed in regards to Agile, user requirements are constantly changing. So the development team needs to modify their features according to their user's often fickle demands.

Whereas the Operations team wants to keep the application as stable as possible. Releasing new features almost always results in more bugs and a less stable system. So Operations teams have historically required various roadblocks to be met by the development team before features can be released. There's well-known (and disliked) aspect of feature-flags. Feature flags have historically resulted in many devestating bugs. We'll talk about 1 well known-case (which isn't entirely about feature-flags). Operations teams might have a restriction on how frequent releases can be or other test-based requirements before the release will happen, arbitrarily slowing down development. This results in the application stagnating.

This friction arises because their goals are partially counter to each other.

The idea of DevOps is to combine these roles into one. Instead of having two teams for each task, we now have 1 team, and all of our developers also understand how Operations works, and Operations-focused team-members can also develop.

## DevOps practices

There are 3 well-known DevOps Practices (occasionally referred to as Agile Practices as well). These are practices we follow to have facilitate and ease the deployment to production. We don't strictly need to follow these practices in order to deploy to production, but they make our lives much better.

The three DevOps Practices are known as CI/CD/CD, however the double CD is often excluded (on top of the fact that most pipelines are only Continuous Delivery), so we typically only say CI/CD.

1. Continuous Integration (CI)
    - The practice of frequently merging multiple developers code together (multiple times a day)
    - Prevents smaller errors/issues from piling up
    - Solve issues when they are still small
2. Continuous Delivery (CD)
    - Automating the deployment process, so that each commit will be tested and prepared for deployment
        - Includes any other step that you want to be part of the process of "pipeline" (Sonarcloud)
    - We automate the process to the point that the release is ready "at the push of a button"
        - Don't actually fully deploy, we just have it ready to go, so that the only thing a human needs to do is "push a button"
    - Involve big "release days"
3. Continuous Deployment (CD)
    - Relates to the "Facebook Model"
    - Every change/commit that successfully passes every stage of the Continuous Delivery Pipeline (listed above) will be immediately released, without waiting for any approval/button press
        - Similar to Continuous Delivery, except even the "button press" is additionally automated
    - With this fully automated process, it becomes more beneficial to push changes very frequently, as you can obtain feedback from your customers/users as soon as possible

Note: "Continuous Development" is _not_ a practice. That is not the correct terminology. I mention this, because it is _very_ common for people to mis-speak and accidentally say "Continuous Development". But ensure you practice the correct terminology, as QC in particular will likely call you out on it.

## Continuous Delivery vs Continuous Deployment

For Continuous Delivery, we want each of our commits/pushes/merges to go through a series of steps of what we call a "pipeline". Each step will consist of something like testing, code quality analysis (sonarcloud), amongst a variety of others.

At the end of our pipeline, we should prepare our release, so that the only action we need to take to fully release this change is with a very simple action. It could be, responding a prompt (yes/no). It could be executing a script. It could be sending an HTTP Request, or any other variety of action. But the key characteristic is that this action has no complexity to it. There is no easy way for human error to prevent this from functioning as expected. We want to minimize the impact of human error. People make mistakes and they are not to be blamed for those mistakes. We should endeavor to update the process so that mistakes cannot break anything.

For Continuous Deployment, we want to not have any interaction with the system or pipeline in order to allow this release to occur. We want to design the system to make its own decisions about the release process. For example, it could just automatically assume the prompt is "yes". It could automatically execute the script, etc.

Continuous Deployment will have every change that successfully passes through the pipeline immediately deployed, without human interaction.
This is not to say that Continuous Deployment cannot have human interaction. It is perfectly reasonable (even suggested) to have abort methods. We should have the ability to interrupt the system if needed. But generally speaking, it will operate without human interaction.

## Continuous Deployment is deceptively difficult

it is perfectly reasonable to point out that Continuous Deployment might lead to higher issues with the production system. The benefits are in terms of velocity of release and the immediacy of user feedback to changes of the system.

It is impossible to act as Facebook does, with releases around every 15 minutes, with a Continuous Delivery process alone.

To combat the downsides, we don't actually mind if our system is not perfect. Our systems do not need to have exactly 100% uptime anyways. 99.99% is perfectly good. As long as our systems meets some SLO like 99.99% or similar, we are happy with some downtime.

However, we typically need to include many additional systems to monitor the performance/behavior of the application as well as advanced deployment strategies to faciliate a full Continuous Deployment process.

# Jenkins

Jenkins is an Open-Source Automation Server. It helps automate the building, testing, and deployment of an application for continuous integration and delivery.

It supports the development of CI/CD or DevOps "pipelines".

We describe them as pipelines because application must go through every step of the pipeline in order to be successfully deployed.

Common steps might be: Compile, test, sonarcloud quality gates, perhaps more tests such as integration tests & performance tests, if you're using docker, you might build the docker image, and deploy a docker container.

## Jenkins Jobs

A Jenkins "Job" represents a single Pipeline. It is used for one project (either frontend or backend; not both) to build/test/deploy etc, generally linked with only 1 Git Repository.

## Jenkins Agents

An Agent is something that will perform an action on behalf of someone/something else. The details depend on what it is an agent _for_. Jenkins is an Automation _Server_. Which means Jenkins Agents can be _other_ servers. They can perform similar tasks, but they will all be managed by the primary Jenkins server.

In other contexts, agents are not entire servers, they could be individual processes/programs.

In our case with Jenkins, we could horizontally scale Jenkins' functionality by configuring additional servers as Jenkins agents.

Each agent (along with the primary jenkins server) will perform the actions configured in different Jenkins Jobs. The primary Jenkins server decides which agent (if any) will be responsible for which currently running jobs. This can change over time, a single job is never automatically associated with a Jenkins agent.

These agents are used to delegate certain tasks when needed. It all comes down to the usefulness of horizontal scaling.

### Horizontal vs Vertical Scaling

As we develop our applications and obtain a larger user-base, it may not be sufficient to have only 1 instance of our program running on 1 server.

Let's say for example that due to the performance of my program and a standard server (2 cpus & 4 GB of RAM), we can only handle 400 requests per second. Since not all users will be sending requests every second, we can likely support more than 400 active users simultaneously, but we will eventually hit this limit. If our application on average sends an HTTP request every 5 seconds, then we can, on average, support only 2000 simultaneous active users.

In the grand scheme of things, 2000 users is barely anything. Facebook, Netflix, Google, etc support millions of similtaneous users constantly throughout every single day.

We want to prepare our application to be able to "scale" to handle more users. Scaling our applications means to change the way it is deployed to facilitate more active users.

Horizontal scaling has to do with making multiple copies/instances of your running program. And similarly, increasing the numbers of servers we deploy to.

Instead of just 1 program running on 1 server, we could have each server run 4 copies of the program, and we can also have 5 servers for a total of 20 instances of our application.

If we do so, we could support 40,000 simultaneous active users.

Vertical scaling is to increase the resources available on the server (make the server "bigger", hence vertical scaling). Instead of 2 cpus and 4 GBs of RAM, we could have 8 cpus and 64 GBs of RAM.

Now our application could support some additional users (however the numbers work out).

At the low end, if you have a very weak server, it is generally quite cheap to just pay for a slightly stronger server. However, you start to see diminishing returns due to cost.

On the low end of performance, it is possible for vertical scaling to be cheaper than horizontal scaling. However, you will quickly find that vertical scaling has an exponential cost growth. So Horizontal scaling is the true efficient route for scaling.

With the onset of cloud platforms such as AWS, horizontal scaling has become even easier to accomplish. AWS and other cloud platforms allow us to create a virtual machine (effectively the same as a real server) within minutes, and they only charge for the exact uptime that the server is being used. When it is not being used, we can shut those servers down, and therefore not have to pay for them.

This provides what we call "Elasticity". Our performance/scaling can both increase and decrease flexibly to save on costs while also scaling to meet demand.

## General Process

In Jenkins, we don't have access to any IDEs or similar to compile/test/run our programs. We need to do this purely through the command line. We need to be familiar with our maven command line tool. We need to be familiar with our ng command line tool, and our git CLI tool, etc.

```bash
chmod +x mvnw
./mvnw clean package # Clean and package the project as a JAR
sudo ps | grep jar-file-name | awk '{print $1}' | xargs kill -9 || true # Kill the previous process by the name of 'jar-file-name'
BUILD_ID=dontKillMe java -jar target/jar-file-name.jar & # Run the new program in the backround
```

We might want to separate out the tests from the package step depending on our configuration. But for a simple pipeline, this is not necessary.

What the above script does, is run the unit tests, package the project as a JAR file, stop the old program, and run the new version of the application in the background.

If any step fails, such as if the unit tests fail or the project fails to be packaged, then the pipeline will not continue. It will stop where it failed.

So the only way we run a new version of the application is if all our tests pass.

The `BUILD_ID=dontKillMe` part of the command is specific to Jenkins, and is needed so that Jenkins does not automatically terminate any background processes.
What we are doing is passing an environment variable called `BUILD_ID` with the value `dontKillMe`, which informs Jenkins that this process should be left alone.

We can modify the instructions/commands in whatever way we want to include additional steps to our pipeline such as submitting analysis to SonarCloud.

## Scripted/Declarative Pipelines

Jenkins supports creating special files known as `Jenkinsfile`s. These are files written in `Groovy` syntax to define how a pipeline will operate. Instead of configuration our Jenkins Job through the Jenkins UI, we can instead configure it through the use of this file. This has benefits in that we can track changes to the pipeline configuration through Git.

Groovy syntax is fairly similar to Java, but also includes some additional language constructs. In particular, Groovy has a JSON like syntax to specific scopes for our Jenkinsfiles. It's not exactly the same as JSON though.

Jenkinsfiles are how Jenkins supports the idea of "Configuration as Code". This principle is the idea of taking our various configurations (in this case, it is our Pipeline config), and represent it also as code. We track with the rest of our codebase, and we have a full changelog history through version control such as Git.

# Sonar-Related Services

So there are 3 tools/services that are under the family known as "Sonar": SonarLint, SonarCloud, and SonarQube.

All 3 are used for similar purposes, which is to provide code quality analysis or related tools.

- SonarLint
    - A `Linter`
        - A piece of software that typically runs inside your IDE
        - This is the tool that creates little red/yellow squigglies under your lines of code
    - It will identify areas of your codebase that could have improved code quality
    - SonarLint identifies what are called "Code Smells" and provides recommendations to improve
- SonarCloud
    - An online web application used to display code quality reports for an application's codebase
    - Reports span over time, and display graphs over time of various code quality metrics
    - Support a free-tier for public repositories, but requires payment for use with private repositories
    - Offers "Quality Gates" which are threshold requirements that must be met by a change in a CI/CD pipeline to pass
        - This allows us to fail our pipeline if the code quality is not sufficient
        - Our pipeline is not simply just tracking functionality, but the actual quality of our code as well
        - This can include test coverage as well
- SonarQube
    - The same software as SonarCloud, except this will be hosted on some private servers somewhere
    - Very useful for companies that do not want information about their codebases stored externally
    - Particularly important when legal matters apply (such as government projects)
- Sonar-Scanner
    - The actual scanner/analysis tool used in SonarLint & SonarCloud
    - We can allow SonarCloud to analyze our code on our behalf or we can run the sonar-scanner tool ourselves and submit reports
        - Running it ourselves is useful to include information about our tests
        - SonarCloud will not run your tests when it analyzes on your behalf