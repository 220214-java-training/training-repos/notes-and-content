## CSS Group Exercise 
Together with your teammates complete the following tasks. You can use any online resource to help you out. Have one person share their screen and lead the exercise while the rest of the team members contribute.

1. Create an HTML file "index.html"
   - Include a heading with id "header"
   - Include an image with id "my-pic"
   - Include two paragraph elements 
   - Include a table
   - Include a button
2. Add inline styling to one of the paragraph tags to change the font color to blue.
3. Add internal styling to the HTML page:
   - change the background color of the body to your color of choice
   - change the font color of all paragraphs 
     - How does the inline rule affect this?
4. Add external styling to the HTML page:
   - define a red border for all img tags
   - define a yellow border for the id "my-pic"
     - What do we notice about the applied styles?
5. Include [bootstrap](https://getbootstrap.com/docs/5.1/getting-started/introduction/) into your webpage using the CSS and JS CDN links.
   - Apply a bootstrap class to style the button.
   - Apply a bootstrap class to style the table.