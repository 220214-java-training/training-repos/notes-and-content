# CSS - Cascading Style Sheets

- css allows us to define styling to change how the html elements on our webpage are rendered
- "cascading" refers to the idea that all of our styles and style sheets will be combined, with qualities "cascading" to elements based on rules and their precedence, and the html hierarchy

### Including Styling in our HTML

1. Inline
   - we can give elements a style attribute, with its value being a string representing a css rule
   - `<div style="text-align:center">... some content</div>`
2. Internal
   - we include css rules in `<style>` tag in the head of our html doc
3. External
   - link to an external css using `link` tag in the head of our html doc
   - `<link rel="stylesheet" type="text/css" href="my-styles.css">`

- precedence of a css rule is based on the way it is applied to the page (e.g. inline v external) and the selector used when defining the rule
- inline styling takes precedence over internal/external styles
  - !important with an external/internal rule can take precedence over inline styling
- internal and external scripts are loaded into the browser in the order they are defined, when there are conflicting styling rules, the later rules override previous styles

### CSS Selectors

all elements

```css
* {
  color: red;
}
```

tagname - sets the color of all paragraphs to red

```css
p {
  color: red;
}
```

class name

```css
.blue-par {
  background-color: blue;
}
```

```html
<p class="blue-par">Some text we want to have a blue background</p>
```

id

```css
#submit-button {
  border-radius: 5px;
}
```

```html
<button id="submit-button">Click to Submit!</button>
```

### Properties and Rules

- background-color: orange;
- color: green;
- font-size: 12;
- border: 6px solid red;
- text-align: center;
- font-family, font-style, font-weight
- padding, margin
- display (inline, block, inline-block)
- list-style-type
- position (static, relative, absolute, fixed, sticky)

## Bootstrap
- bootstrap is a CSS framework that allows you to create responsive web pages using pre defined classes and styling
- more on bootstrap [here](https://getbootstrap.com/)