const newCustomerForm = document.getElementById("new-customer-form");
newCustomerForm.addEventListener("submit", (e)=>{
    // The default behavior for a form submission, is to make a  
        // synchronous HTTP request. Because we intend to make this 
        // request asynchronous, we want to prevent this default behavior.
    e.preventDefault();
    console.log("form was submitted");

    // take the data from the input
    const nameInput = document.getElementById("name-input");
    console.log(nameInput.value);
    const emailInput = document.getElementById("email-input");
    console.log(emailInput.value);

    // create a POST request, sending it to the server with the data
    const xhr = new XMLHttpRequest();
    xhr.open("POST", "http://localhost:8080/customers");

    const messagePar = document.getElementById("result-message");
    xhr.onreadystatechange = function(){
        if(xhr.readyState===4 && xhr.status === 201){
            // indicate to the user that the request was successful
            console.log("successfully added new record!");
            messagePar.hidden = false;
            messagePar.innerText = "You successfully added a new customer!"
        } else if (xhr.readyState===4) {
            messagePar.hidden = false;
            messagePar.innerText = "Something went wrong :("
        }
    }

    const newCustomer = {name: nameInput.value, email: emailInput.value};
    const customerJson = JSON.stringify(newCustomer);

    xhr.setRequestHeader("Content-Type", "application/json");

    xhr.send(customerJson);

})


