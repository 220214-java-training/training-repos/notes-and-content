const xhr = new XMLHttpRequest();

xhr.open("GET","http://localhost:8080/customers");

xhr.onreadystatechange = function(){
    if(xhr.readyState===4){
        // obtain the customer data from the response
        const responseBody = xhr.response;
        // convert JSON response body into JS objects
        const customerArray = JSON.parse(responseBody);
        renderCustomers(customerArray);
    }
}

xhr.send();

// renderCustomers should take an array of JS objects 
    // and render list items on our web page, based on each item
function renderCustomers(customers){

    const customerList = document.getElementById("customer-list");

    for(let customer of customers){
        const newListItem = document.createElement("li");
        newListItem.classList.add("list-group-item");
        newListItem.innerText = ` ${customer.name} --- ${customer.email} `;
        console.log(newListItem);
        customerList.appendChild(newListItem);
    }

}
