fetch("http://localhost:8080/customers") // makes an HTTP request to our server, returns a promise of a response
.then(resp=>resp.json()) // obtain the JSON response body
.then(data=>{ // process the data obtained from the response body
    console.log(data);
    // render data on web page
    renderCustomers(data);
})
.catch(e=>console.log(e));


function renderCustomers(customers){

    const customerList = document.getElementById("customer-list");

    for(let customer of customers){
        const newListItem = document.createElement("li");
        newListItem.classList.add("list-group-item");
        newListItem.innerText = ` ${customer.name} --- ${customer.email} `;
        console.log(newListItem);
        customerList.appendChild(newListItem);
    }
    
}