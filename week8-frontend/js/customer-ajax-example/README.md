### Customer AJAX Example
- our goal is to make an asynchronous request using AJAX
- the request should be sent to our spring boot application [spring-data-jpa-demo](https://gitlab.com/220214-java-training/training-repos/spring-data-jpa-demo)
- the response will be handled using JavaScript, and we will dynamically render the data received from our server