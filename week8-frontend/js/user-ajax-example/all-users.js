// make an asynchronous http request for all of the user data
const xhr = new XMLHttpRequest();

xhr.open("GET", "https://jsonplaceholder.typicode.com/users");

xhr.onreadystatechange = function(){
    // console.log("a ready state has changed. current ready state: "+xhr.readyState);
    if(xhr.readyState===4){
        // do something with the response
            // console.log("response is done!")
        // access the response body
        const responseBody = xhr.response;
        // JSON parse converts JSON into JS objects
            // JSON.parse: JSON -> JS
            // JSON.stringify: JS -> JSON
        const userArray = JSON.parse(responseBody);
            // console.log(userArray);
        renderUserInfo(userArray);
    }
}

xhr.send();

// render that data on the web page
function renderUserInfo(users){
    const tableBody = document.getElementById("user-table-body");

    // iterate over each user
    for(let user of users){
        const newRow = document.createElement("tr");

        const nameData = document.createElement("td");
        nameData.innerText = user.name;
        newRow.appendChild(nameData);

        const emailData = document.createElement("td");
        emailData.innerHTML = user.email;
        newRow.appendChild(emailData);

        const phoneData = document.createElement("td");
        phoneData.innerText = user.phone;
        newRow.appendChild(phoneData);

        tableBody.appendChild(newRow);
    }

}