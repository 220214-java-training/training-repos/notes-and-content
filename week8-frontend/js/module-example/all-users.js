import { ajaxGet } from "./ajax-requests.js";

const userUrl = "https://jsonplaceholder.typicode.com/users";

ajaxGet(userUrl, renderUserInfo);

// render that data on the web page
function renderUserInfo(xhr){
    const users = JSON.parse(xhr.response);

    const tableBody = document.getElementById("user-table-body");

    // iterate over each user
    for(let user of users){
        const newRow = document.createElement("tr");

        const nameData = document.createElement("td");
        nameData.innerText = user.name;
        newRow.appendChild(nameData);

        const emailData = document.createElement("td");
        emailData.innerHTML = user.email;
        newRow.appendChild(emailData);

        const phoneData = document.createElement("td");
        phoneData.innerText = user.phone;
        newRow.appendChild(phoneData);

        tableBody.appendChild(newRow);
    }

}