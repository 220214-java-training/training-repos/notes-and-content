function ajax(method, url, callback, payload, contentType){

    const xhr = new XMLHttpRequest();

    xhr.open(method, url);

    xhr.onreadystatechange = function(){
        if(xhr.readyState===4 && xhr.status > 199 && xhr.status < 300){
            callback(xhr);
        }
    }

    if(contentType){
        xhr.setRequestHeader("Content-Type", contentType);
    }

    if(payload){
        xhr.send(payload);
    } else {
        xhr.send();
    }

}

function ajaxGet(getUrl, getCallback){
    ajax("GET", getUrl, getCallback);
}

function ajaxPost(postUrl, postCallback, payload, contentType){
    ajax("POST", postUrl, postCallback, payload, contentType);
}

export { ajaxGet, ajaxPost };