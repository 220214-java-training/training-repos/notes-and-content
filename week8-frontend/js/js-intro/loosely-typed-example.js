// console.log('Hello World');

/// exploring the loosely typed nature of JS
/*
console.log("7"+7); // number 7 is coerced into a string value
console.log(7+7+"7");  // 14 + "7"  -> 147; 14 is coerced into a string value
console.log("7"+7+7);  // first we get "7"+7-> "77", then we get "77"+7 -> "777"

console.log("7"*7); // string "7" is coerced into a number value
console.log(7+"bananas");
console.log(7*"bananas"); // NaN (placeholder for a numeric value when we can't represent it)
*/

/// == vs === 
    // == checks value equality (allows for type coercion)
    // === checks both value and type equality
/*
console.log("7=='7': "+(7=='7'));
console.log("7==='7': "+(7==='7'));
*/


/// truthy and falsy values (another application of type coercion)
    // falsy: null, undefined, "", 0, false, NaN
    // truthy: anything else
/*
console.log("truthy/falsy null: "+ Boolean(null));
console.log("truthy/falsy 0: "+ Boolean(0));
console.log("truthy/falsy 20: "+ Boolean(20));
console.log("truthy/falsy 'hello world': "+ Boolean('hello world'));
console.log("truthy/falsy true: "+ Boolean(true));
console.log("truthy/falsy {}: "+ Boolean({}));

let loggedInUser = 5; // = {id:3, username:"crehm", displayName:"Carolyn"};
if(loggedInUser){
    console.log("Welcome "+loggedInUser.displayName+"!");
} else {
    console.log("please log in");
}
*/