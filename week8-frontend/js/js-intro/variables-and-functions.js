// using the var keyword
/*
var x = 5;
var x = 10;
console.log(x);
*/

// using the let keyword
/*
let x = 5;
let x = 10; // this gives us an error in our browser 
console.log(x);
*/

/*
let x = 5;
x = 10;
console.log(x);
*/

// using the const keyword
/*
const y = 12;
y = 20; // this gives us an error in our browser
*/

//// creating functions 
/*
let sum = 0;
function addNums(n1,n2){
    sum = n1 + n2;
    console.log(sum);
    return sum;
}

let addNums = function(n1, n2){
    return n1+n2;
}

let result = addNums(3,4);
console.log(result);
console.log(sum);
*/

// let addNums = (n1,n2) => {return n1+n2};
let addNums = (n1,n2) => n1+n2;
let result = addNums(3,4);
console.log(result);

let squareNumber = n => n*n;
let squareResult = squareNumber(4);
console.log(squareResult);
