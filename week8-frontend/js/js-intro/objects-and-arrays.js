/// creating objects
    // creating an object literal
let loggedInUser = {id:3, username:"crehm", displayName:"Carolyn"};
    // we can use either bracket or dot notation to access/set fields
// console.log(loggedInUser["id"]);
// console.log(loggedInUser.username);
loggedInUser.isAdmin = true;
console.log(loggedInUser);
// console.log(loggedInUser.email); // this is undefined

// greeting is a method of the loggedInUser
    // we can also set fields to be functions
loggedInUser.greeting = function(){ 
    console.log("Welcome "+this.displayName)
};
console.log(loggedInUser);

    // we can execute functions by accessing them with dot notation
loggedInUser.greeting();

for(let x=0; x<10; x++){
}

// let loggedInUser = {id:3, username:"crehm", displayName:"Carolyn"};
// iterating over an object
    // for/in allows us to iterate over the fields of any object
for(let field in loggedInUser){
    console.log(field +" : "+loggedInUser[field]);
}

/// working with arrays 
    // for/in allows us to iterate over the index of arrays
let myArray = [5, true, "hello world", ":)"];
for(let i in myArray){
    console.log(i +" : "+myArray[i]);
}

    // for/of allows us to iterate over the values of arrays
for(let val of myArray){
    console.log(val);
}


