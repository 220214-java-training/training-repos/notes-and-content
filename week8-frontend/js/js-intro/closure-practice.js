// anything declared in a function scope (including other functions) have access
    // to any variables that are declared in the same scope
function addNums(n1,n2){
    let sum = n1 + n2;
    console.log(`printing sum from 'addNums': ${sum}`);
    function printSum(){
        console.log(`printing sum from 'printSum': ${sum}`);
    }
    printSum();
    return sum;
}

let otherCountVar = 0;

function counterFunction(){
    let countValue = 0;
    function increment(val){
        if(val){
            countValue+=val;
        } else {
            countValue+=1;
        }
        return countValue;
    }
    return increment;
}


function counterFunction2(){
    let countValue = 0;
    return function(val){
        if(val){
            countValue+=val;
        } else {
            countValue+=1;
        }
        return countValue;
    }
}


// let u = new User();
function User(){
    let _id;
    let _username;

    this.getId = function(){
        return _id;
    }

    this.setId = function(id){
        _id = id;
    }

    this.getUsername = function(){
        return _username;
    }

    this.setUsername = function(username){
        _username = username;
    }

    this.display = function(){
        console.log(`id: ${_id}, username: ${_username}`)
    }

}