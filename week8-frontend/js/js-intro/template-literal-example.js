const name = "Carolyn"
let myStr = "hello world";
let myStr2 = 'hello world, '+name;
// template literals allow us to define multiline strings
let myTemplateLiteral = `hello 

world`;

const loggedInUser = {id:3, username:'crehm', displayName: 'carolyn'};
let greetingString = "Hello user #"+loggedInUser.id+", named "+loggedInUser.displayName+"!";

// they also let us include variables right into our string without the need
    // for string concatenation
let greetingTemplateLiteral = `Hello user #${loggedInUser.id}, named ${loggedInUser.displayName}!`;
console.log(greetingTemplateLiteral);