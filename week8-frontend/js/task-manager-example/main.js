const tasks = [
    {id:1, description: "take out trash", priority: "HIGH"},
    {id:2, description: "wash dishes", priority: "MEDIUM"},
    {id:3, description: "fold laundry", priority: "LOW"},
    {id:4, description: "study for QC", priority: "HIGH"},
    {id:5, description: "feed cat", priority: "MEDIUM"}
]

for(let task of tasks){
    //console.log(task.description);
    const newListItem = document.createElement("li");
    newListItem.innerHTML = task.description;

    newListItem.addEventListener("click", ()=>{
        // if the list item has already been marked as completed 
        if(newListItem.classList.contains("completed")){
            // remove the completed class
            newListItem.classList.remove("completed");
        } else {
            // if the list item is not marked as complete, mark it as complete
            newListItem.classList.add("completed");
        }
    })

    document.getElementById("task-list").appendChild(newListItem);
}