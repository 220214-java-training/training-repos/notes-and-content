class Dog {
    name;
    breed;
    age;
    owner;
    
    constructor(name, breed, age, owner){
        this.name = name;
        this.breed = breed;
        this.age = age;
        this.owner = owner;
    }

    bark(){
        console.log("woof");
    }

    display(){
        console.log(`name=${this.name}, breed=${this.breed}, age=${this.age}`)
    }

}

class Person {
    name;
    email;
    phone;

    constructor(name,email,phone){
        this.name = name;
        this.email = email;
        this.phone = phone;
    }

    display(){
        console(`name=${this.name}, email=${this.email}, phone=${this.phone}`);
    }

}

