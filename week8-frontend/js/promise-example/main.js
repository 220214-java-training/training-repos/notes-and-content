let request = new Promise((resolve, reject) => {
    let xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
        if(this.readyState === 4 && this.status === 200) {
            resolve(this.responseText);
        } else if(this.readyState === 4 && this.status > 399) {
            reject(new Error('The request did not succeed'));
        }
    };
    
    xhr.open('GET', 'https://pokeapi.co/api/v2/pokemon/arcanine2');

    xhr.send();
}).then((result) => {
    console.log(JSON.parse(result));
}).catch((error) => console.log(error));