const subheaders = document.getElementsByTagName("h3");
subheaders[0].innerHTML = "here are some of my favorite toppings:";

const newListItem = document.createElement("li");
const toppingList = document.getElementById("topping-list");
console.log(toppingList);
newListItem.innerHTML = "whipped cream";
toppingList.appendChild(newListItem);

const pictureElement = document.getElementById("pancake-pic");
pictureElement.src = "https://dinnersdishesanddesserts.com/wp-content/uploads/2020/03/Strawberry-Shortcake-Pancakes.jpg";
// pictureElement.setAttribute("src", "https://dinnersdishesanddesserts.com/wp-content/uploads/2020/03/Strawberry-Shortcake-Pancakes.jpg")


pictureElement.addEventListener("mouseover", enlargePhoto)

function enlargePhoto(){
    pictureElement.height = 500;
    pictureElement.width = 312;
}

pictureElement.addEventListener("mouseleave", ()=>{
    pictureElement.height = 400;
    pictureElement.width = 250;
})

document.getElementById("text-color-button").addEventListener("click", ()=>{
    const headerText = document.getElementById("header-text");
    if(headerText.hasAttribute("style")){
        headerText.removeAttribute("style");
    } else {
        headerText.style = "color:red";
    }
})