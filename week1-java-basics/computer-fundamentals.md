# Computer Fundamentals

A computer is nothing more than an electronic device that takes input, stores and processes that input, and produces an output.

## Basic Architecture 

![basic architecture](https://d3jlfsfsyc6yvi.cloudfront.net/image/mw:1024/q:85/https%3A%2F%2Fhaygot.s3.amazonaws.com%3A443%2Fcheatsheet%2F29363_64227b1f2b6f41cbab34ba44be766123.png)

(from [toppr.com](https://www.toppr.com/ask/content/concept/computer-and-its-parts-210659/))

CPU is often referred to as the "brain of computer" and is responsible for executing operations.

Computers have primary and secondary storage. Primary storage includes random access memory and read only memory. Random access memory (RAM) is used for short term memory when a computer is running tasks and application. This memory actively stores application data as long as there is constant power in the system. If the machine shuts down, anything stored here is lost. Read only memory (ROM) permanently stores instructions for your computer and cannot be changed by the user.

Secondary storage includes Hard Disk Drives (HDD) & Solid-State Drives (SSD). These drives are used to store persistent data. Unlike RAM, if the system loses power, information stored in secondary storage is retained. Secondary storage can also include external storage devices.

We define software to utilize this hardware, and perform useful operations. 

## Operating Systems

System software includes the computer's operating system. The operating system launches and manages all of the other application software on a computer. Operating Systems provide a UI and abstract away low level functionality for our applications.  Our OS sits between the hardware of our machine and the application software to facilitate consistent interactions between the two.

![os](https://cdn.ttgtmedia.com/rms/onlineimages/whatis-how_operating_systems_work.png)

Users can interact with the OS of our machine in a few different ways. Application software interacts with the OS through an API. We can use a terminal to pass input to a shell, to interface with the computer's os using text commands.

![shell](https://dwmkerr.com/effective-shell-part-5-understanding-the-shell/images/diagram3-terminal-and-shell-1.png)

## Bash Commands 

#### Arguments and Flags
In Unix, commands can be modified in two ways.
1. Commands can have arguments. Arguments given to a command take the form of strings written after the command. ex. `command arg1 arg2 arg3`
2. Commands can have flags. Flags are special arguments given to a command. There are two kinds of flags in Unix, short-hand or character flags, a single character (or group of characters), prefixed by a single dash `-c`, and full flags, the full name of the flag, prefixed by a double dash `--flag`.

Arguments to your command usually represent variables or targets for your command, and flags usually represent options you wish to enable for your command.

Ex. The following two commands do the same thing:
```
cp -r hi bye
cp --recursive hi bye
```
The above commands copy the contents of the `hi` directory to the `bye` directory recursively, using the `-r` and `--recursive` flags. This means that the `cp` command has been told to copy the contents of a directory instead of it's default mode which copies a specific file.

`hi` and `bye` are arguments to the command.

#### Directory Commands
* `cd` - The change directory command allows us to navigate to a different directory on the drive. 
  * go to root directory: `cd /`
  * go to home directory: `cd` or `cd ~`
  * navigate one directory up: `cd ..`
  * navigate into the `hi` directory, which is inside the `bye` directory: `cd ./bye/hi`
  * change to the previous directory: `cd -`
* `ls` - The list directory command allows us to see the contents of a particular directory. When given no arguments, it lists the contents of the current directory. The `-a` flag allows you to see hidden items in the directory.
  * list the contents of the current directory: `ls`
  * list the contents of the `hi` directory: `ls hi` or `ls ./hi`
  * list the contents of the directory including the "hidden" contents: `ls -a`
* `mkdir` - The make directory command allows us to create a new directory. mkdir takes an argument representing the name of the directory you wish to create.
  * create a directory named `hi`: `mkdir hi`
* `pwd` - The print working directory command prints the full name of the directory you are currently working in. For example, if you were working in the `home` directory inside of the `root directory` the output of `pwd` might be `/home`.

#### General Purpose Commands

* `clear` - the clear command usually prints a number of blank lines such that all previous commands are no longer on the screen. There is a shortcut for this command, `ctrl-l`
* `echo` - the echo command will print a string or the result of a command to the console.
* `>` - The `>` operator will redirect the output of a command to a file. The file will be created or overwritten if it already exists. ex. `ls . > log.txt`
* `>>` - The `>>` operator acts the same way as the `>` operator but appends output to the file instead of overwriting if it exists.
* `grep` - the grep command prints any lines in a file or files that match a given pattern. By default, grep interprets the pattern as a basic regular expression.
  * Print all lines in `hello.txt` that contain the word `goodbye`: `grep goodbye hello.txt`

#### File Commands
* `cat` - the concatenate command prints the contents of a file to the console. `cat hello.txt`
* `head` - the head command prints the first ten lines of a file to the console. `head hello.txt`
* `tail` - the tail command prints the last ten lines of a file to the console. `tail hello.txt`
* `touch` - the touch command allows you to modify the timestamp of a file. This command is usually used to create empty files, as an empty file is created if touch is given a file name that does not exist. `touch hello.txt`
* `cp` - the copy command creates a copy of the specified file at the location specified. If the recursive glag is used, it will operate on directories.
  * copy a `hello.txt` to `goodbye.txt`: `cp hello.txt goodby.txt`
  * copy the `hello` directory to the `goodbye` directory: `cp -r hello goodbye`
* `mv` - the move command will rename or move a file or entire directory with the recursive flag.
  * rename a `hello.txt` to `goodbye.txt`: `mv hello.txt goodbye.txt`
  * move `hello.txt` to the `goodbye` directory: `mv hello.txt goodbye/.`
  * rename the `hello` directory to `goodbye`: `mv -r hello goodbye`
* `rm` - the remove command will delete a file. If you use the recursive flag, it can delete a directory. The force flag will cause the command to delete files without prompting the user if there are warnings. The command `rm -rf .` is extremely dangerous.
  * remove `hello.txt`: `rm hello.txt`
  * remove the `hello` directory: `rm -r hello`

## Command Line Text Editors 

Command line text editors allow you to modify the contents of a file without leaving your terminal. 

**nano**

A bit more beginner friendly. If you've never worked with a command line text editor before, it's a more intuitive to learn the basics. 
- [Beginner's guide to nano](https://www.howtogeek.com/howto/42980/the-beginners-guide-to-nano-the-linux-command-line-text-editor/)

**vim**

If you're feeling a bit more ambitious, vim is a powerful command line text editor that can be used very efficiently. Vim has several different modes for interacting with your text.
- [Beginner's guide to vim](https://www.linux.com/training-tutorials/vim-101-beginners-guide-vim/)
