## Web Service Review Questions
- What is a web service?
- What are the benefits of using web services?
- What is "service oriented architecture"?
- What is REST?
- What are the constraints associated with REST?
- Explain the naming conventions for a RESTful web service?
- What does it mean for a web service to be stateless?
- What are the benefits of a stateless web service?
- What is the difference between a monolith and microservice architecture?
- What are the benefits of using microservices over a monolith?
- What are the benefits of using a monolith over microservices?

## Spring Intro Review Questions
- What is Spring?
- What is dependency injection?
- What are the benefits of DI?
- What is meant by the "IOC container"?
- How can we register a bean with the IOC container?
- What are the different stereotype annotations? Where do we use each?
- How can we configure spring to perform DI?
- How does spring resolve autowired dependencies? (name of the variable, type associated with the dependency, etc)
- What is a Spring module? What is a Spring project?
