# EventEmitters

A way of emitting custom events synchronously and asynchronously. Also allowing us to register handler methods for those events.

## Example
Created a count field of type number in the AppComponent. Defined the input as the count and the output was an increment/decrement.
Then defined an event handler of type number. It included an incremement and decrement method. In each method it leveraged the emit method. And send it back to the parent component to be registered on the event object. In this example the events were clicks on the parent component.

EventEmitters are a feature of Angular that allow communication between parent and child components. They support passing data and providing events to the parent that the parent can respond to.

They primarily function through the use of the `@Input()` and `@Output()` decorators.
The `@Input()` decorator defines properties of the component that must be bound via property binding in the parent to pass data in.

Parent Component:
```html
<child-component [field]="someData" (emitter)="respond($event)"></child-component>
```

```typescript
@Component({
    ...
})
export class ParentComponent {
    someData: number = 15;

    respond(data: number) {
        console.log("The new value is " + data);
        this.someData = data;
    }
}
```

Child Component:
```html
<p>{{ field }}</p>
<button (click)="modify()">Increment</button>
```

```typescript

@Component({
    ...
})
export class ChildComponent {

    @Input("field") field: number;

    @Output() emitter: EventEmitter<number> = new EventEmitter<number>();

    modify(): void {
        this.emitter.emit(this.field + 5);
    }
}
```

[Helpful video](https://www.youtube.com/watch?v=XNVjxTVGV8A)
[Another Helpful Video](https://www.youtube.com/watch?v=sC8zVGywhes)
[Documentation](https://angular.io/api/core/EventEmitter)

# Pipes

Pipes are a way to provide functionality to your templates to change the way certain information is displayed without changing the underlying data/information.

There are some built-in pipes and you can create custom pipes.
Some built-in pipes:
number
date
uppercase
lowercase
currency
decimal
percent
slice
titlecase

These pipes have some configurations like how many decimal places or how much of the whole number should be displayed for the decimal pipe.

The pipe class has a method called `transform` that is invoked when you use a pipe in a template.

Typically, pipes are used in the template file for your components in conjunction with String Interpolation

```html
<p>{{ todaysDate | date:'d/M/y' }}</p>
```

```typescript
@Component({
    ...
})
export class AppComponent {
    todaysDate = new Date();
}
```

You can also chain multiple pipes in sequence inside your template.
For example, we could further piped our Data to an uppercase pipe:

```html
<p>{{ todaysDate | date:'d/M/y' | uppercase }}</p>
```

To create a custom pipe, Angular uses the `@Pipe` decorator:

## Example Custom Pipe

```typescript
import {Pipe, PipeTransform} from '@angular/core';
@Pipe({
   name : 'sqrt'
})
export class SqrtPipe implements PipeTransform {
   transform(val: number): number {
      return Math.sqrt(val);
   }
}
```

### Quick aside about the idea of pure pipes
{
    field1: 5,
    field2: "Larry"
} (Memory location 1000)

->

{
    field1: 10,
    field2: "Larry"
} (Memory location 1000)
(The pipe would not execute again)

If you leave the original object alone, and only modify the fields on the object

myObject.field1 = 10

vs.

{
    field1: 5,
    field2: "Larry"
} (Memory location 1000)

->

{
    field1: 15,
    field2: "Larry"
} (Memory location 2000)
(The pipe would execute again)

This would happen if you use the assignment operator on the entire object:

myObject = {
    ...
}

[Angular Guide](https://angular.io/guide/pipes)
[Example from TutorialsPoint](https://www.tutorialspoint.com/angular4/angular4_pipes.htm)