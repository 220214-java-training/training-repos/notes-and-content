# Databinding

Databinding is a concept in Angular to bind data between the typescript file and the html file.

Angular supports 4 different ways of databinding.

## String Interpolation

String interpolation is the simplest technique for databinding. It allows binding one-way from the typescript file to the html file.

`.ts` -> `.html`

### Syntax

Uses 2 pairs of curly braces in the innerHTML of an element in the template file.
Note that spaces inside a curly brace don't typically matter, but spaces between them do.

```html
<p>Hi, my name is {{ firstName }} {{ lastName }}</p>
```

If the typescript file looked as follows:

```typescript
@Component({
    selector: 'app-custom',
    templateUrl: './custom.component.html',
    styleUrls: ['./custom.component.css']
})
export class CustomComponent {
    firstName = 'Larry';
    lastName = 'Potter';
}
```

This would render in your UI as "Hi, my name is Larry Potter".

## Property Binding

Sometimes we may want to bind data not to the innerHTML but to a attribute of an html element. It allows binding one-way from the typescript file to the html file.

`.ts` -> `.html`

Common examples include binding a dynamic URL to an image tag.

### Syntax

Uses a single pair of square brackets around the attribute name. And the value of the attribute must be a variable on the typescript class.

```html
<img [src]="imageUrl" />
```

If the typescript class looked as follows:

```typescript
@Component({
    selector: 'app-custom',
    templateUrl: './custom.component.html',
    styleUrls: ['./custom.component.css']
})
export class CustomComponent {
    imageUrl = 'https://upload.wikimedia.org/wikipedia/commons/6/6e/Harry_Potter_wordmark.svg'
}
```

This would render as a image of the Harry Potter title in its well-known font, found at the provided url:

![Harry Potter Title](https://upload.wikimedia.org/wikipedia/commons/6/6e/Harry_Potter_wordmark.svg)

## Event Binding

Event Binding is the first type of databinding binds in the opposite direction. It allows Angular to receive input from user events.
It allows binding from the html file to the typescript file.

`.html` -> `.ts`

Examples include clicking or mouseover or focus events, etc.

### Syntax

Uses a single pair of parentheses around the lowercase name of the event.
The event names are similar to static HTML events, except the "on" prefix is removed.
click
mouseover
focus
submit
etc

The value of the attribute is a method call that exists on the component class.

```html
    <button type="submit" (click)="submit()">Submit</button>
```

If the typescript class looked as follows:

```typescript
@Component({
    selector: 'app-custom',
    templateUrl: './custom.component.html',
    styleUrls: ['./custom.component.css']
})
export class CustomComponent {
    submit(): void {
        // The method body
        // Do whatever you need it to do
        console.log("The button has been clicked");
    }
}
```

This would result in the message "The button has been clicked" being logged to the console every time the button is clicked.

## Two-Way Databinding

Allows us to bind bi-directionally. We can both retrieve input from the user of the application and re-display what they have entered back to the UI.

`html` <-> `.ts`

This type of databinding only works on `input` elements of the template file.
It also additionally requires an additional module import of `FormsModule` in your `AppModule`.

### Syntax

Uses a combination of the syntaxes for property binding and event binding together.
Square brackets on the outside and parentheses on the inside. The attribute that they surround is a custom attribute that must be "ngModel".
The value of this custom attribute is the name of a variable on the component class which stores the value of the user input.
This can then be re-bound back to the frontend to display what the user is dynamically typing.

```html
<input type="text" [(ngModel)]="userInput" />
<p>You have entered: {{ userInput }}</p>
```

If the typescript class looked as follows:

```typescript
@Component({
    selector: 'app-custom',
    templateUrl: './custom.component.html',
    styleUrls: ['./custom.component.css']
})
export class CustomComponent {
    userInput: string = '';
}
```

This will render as an empty input field with a paragraph below that shows that the user has yet to type anything.
As the user enters anything into the input field, the paragraph will dynamically update with what they type.

# Directives

Directives in Angular allow for a lot of dynamic functionality. They are quite broad in what they can accomplish, but examples include custom html elements, or custom html attributes. And each of these can perform any action you are capable of programming.

Examples of built-in/provided Directives:
Components themselves.

Other Directives are often sub-categorized as either: Structural Directives or Attribute Directives.

Structural Directives are named because they modify the structure of your template/html files.
These Structural Directives are prefixed with an asterisk.
Examples:

`*ngFor`
`*ngIf`
`*ngSwitch` & `*ngSwitchCase`

Attribute Directives do not have a prefix and often modify the display/style of elements.

`ngClass`
`ngStyle`

These directives support dynamic ways to modify the style/classes of various components without having to directly interface with the DOM. These are beneficial, as they do support various performance optimizations that Angular has implemented.

Together, these directives provide many ways to provide dynamic functionality to your web application.

We can use an `*ngFor` to render a row in a table for every reimbursement a user has.
We could use an `*ngIf` to display/hide certain elements based on whether a user is logged in or not.

### Example Syntax

```html
<table>
    <tr>
        <th>Index</th>
        <th>ID</th>
        <th>Amount</th>
        <th>Author</th>
        <th>Status</th>
        <th>Resolver</th>
        <th>Description</th>
    </tr>
    <tr *ngFor="let reimbursement of allReimbursements; let i = index">
        <td>{{ i }}</td>
        <td>{{ reimbursement.id }}</td>
        <td>{{ reimbursement.amount }}</td>
        <td>{{ reimbursement.author }}</td>
        <td>{{ reimbursement.status }}</td>
        <td>{{ reimbursement.resolver }}</td>
        <td>{{ reimbursement.description }}</td>
    </tr>
</table>
```

If the typescript class looked as follows:

```typescript
@Component({
    selector: 'app-custom',
    templateUrl: './custom.component.html',
    styleUrls: ['./custom.component.css']
})
export class CustomComponent {
    allReimbursements: Reimbursement[] = [
        new Reimbursement(/* some data */),
        new Reimbursement(/* some data */),
        new Reimbursement(/* some data */),
        new Reimbursement(/* some data */),
        new Reimbursement(/* some data */),
        new Reimbursement(/* some data */)
    ];
}
```

This will render a table of Reimbursement data. Where each row renders the data of each individual reimbursement.

```html
<nav class="navbar navbar-expand-lg navbar-primary bg-dark">      
    <ul class="nav nav-fill justify-content-center">
        <li class="nav-item">
            <a class="nav-link active" aria-current="page"><button class="btn btn-primary">Dashboard</button></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#"><button class="btn btn-primary">Loan Tools</button></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#"><button class="btn btn-primary">Contacts</button></a>
        </li>
        <li class="nav-item" *ngIf="isLoggedIn">
            <a class="nav-link" href="#"><button class="btn btn-primary">Logout</button></a>
        </li>
    </ul>
</nav>
```

If the typescript class looked as follows:

```typescript
@Component({
    selector: 'app-custom',
    templateUrl: './custom.component.html',
    styleUrls: ['./custom.component.css']
})
export class CustomComponent {
    isLoggedIn: boolean = true;
}
```

This will render a navbar with a optional Logout link.
This logout link will only be rendered while the `isLoggedIn` property is true.

The neat part about *ngIfs are that they are not simply a `hidden` property.
While the *ngIf conditional is false, the entire element literally will not exist.

# Observables

Observables come from a library used by Angular called `RxJs` which stands for `Reactive Extensions for JavaScript`.
This provides operations to handle asynchronous events/data in a fairly similar fashion to Promises with a notable differences.

Promises will end after they either resolve or reject. They are one and done.
Observables potentially support multiple values passing through a single Observable.
I generally envision some sort of data pipeline or sequence of data values passing through the Observable.

Not every Observable always supports multiple values, but it is an option.

Observables also support multiple "subscribers".

Promises leverage the `.then` and `.catch` methods but it is difficult to have multiple different sources respond to the same promise. (Not impossible however).

Observable objects include a `.subscribe` method that will be invoked every time a value is "emitted" from the Observable.
Multiple different sources can each invoke the `.subscribe` method, and they will each be invoked every time.

Observables have a sort of extension known as a `Subject`.

Subjects are interesting because not only can they support multiple subscribers, but they can also support multiple sources of input.
This supports a well-known pattern/architecture known as the "Publisher/Subscriber" pattern. In general, the concept is that you support many instances of input and many instances of output.

Unlike Promises, Observables unfortunately do not support the built-in syntax of `async` and `await`.

Observables exist in several "states".
Observables will continue to "emit" any values that pass through until the Observable "completes". Once a Observable "completes", it is not too different from a promise.

Due to this, there are ways to convert Observables into Promises, but typically they only really work for Observables that eventually complete.

Some Observables never complete at all, which are commonly referred to as "Infinite Observables".
Observables include a `.toPromise` method, but it is being deprecated in favor of alternative methods that are more understandable.
Since the name `toPromise` does not make it very clear that it only works for Observables that will complete.
In version RxJs 6, the new methods are not yet available, and they are released in RxJs 7.
The primary new method is called `lastValueFrom`, which makes this much more clear.

## Functional Paradigm

The idea of the "Functional" Paradigm is that functions are closer to the concept of mathematical functions.
These functions have inputs and outputs and that's generally it.

There a special classification of functions that produce what are called "side effects".
If you have ever written a function that prints something to a console, this is an example of a "side effect".
It is something that happened that was not part of the "output" or return value.

Void functions are typically functions that create side effects.

Additionally, in the functional approach, we typically do not modify data very frequently.
Instead, we produce new data that is different in some way.

Instead of Sorting an array, we just simply construct a new array that is sorted. This way, we have both the original, unsorted array, and the new, sorted array.

If we didn't do this, we would only have the single array, that is sorted.

We continue this process to follow a sort of chain if inputs/outputs.

``` typescript
If we start with an array of numbers [1, 2, 3, 4, 5]:

We would have syntax similar to this:
Observable.of(array).pipe(
    map(element => element * 2), // doubles each value
    map(element => element + 1), // add 1 to each value
    tap(element => console.log(element)), // produce a side effect, printing each value to the console
);
```

The idea is that we follow a sequence of instructions, and we are producing new data each time.

`map` is a well-known functional operation, that converts each input of an array to an output. Each input is modified in some way.
`tap` is a well-known functional operation, that produces side effects. It does not modify/produce any data
`reduce` is a well-known functional operation, that combines an array of data into a single value
Examples for reduce might be to add everything together or to compute the average, or something.

#### Quick aside for JavaScript/TypeScript anonymous functions

```typescript
input => output
// Is a function with a single input that returns output

input => {
    console.log(input);
}
// If you include curly braces, there is no default return value, so the above would be a void function

input => {
    return output;
}
// Is functionally identical to the first example of input => output
```

Additionally, for ease of clarity and readability, we wrap the inputs in parentheses.

```typescript
(input) => output

(input) => {
    console.log(input);
}

(input) => {
    return output;
}
```