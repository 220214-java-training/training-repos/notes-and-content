# Angular

Angular is a Component-Driven framework for developing Web Applications created by Google. Specifically, it is used to create "Single Page Applications" (SPA).
Angular is developed in and uses TypeScript, which is a superset of JavaScript. Everything that JavaScript can do, so can TypeScript. And TypeScript has additional features that JavaScript does not (namely, support for types). TypeScript has a feature called "Decorators" which are very similar to Annotations in Java. Angular leverages these Decorators for its configuration.

Angular follows the Model-View-Controller (MVC) Design Pattern, similar to Spring Framework.

## Single Page Applications

Websites have traditionally consisted of multiple HTML files, each of which must be requested from the web-server. Every single request takes a small amount of time. Over the course of using the website, there is a lot of lost time waiting for new pages to load.

Single Page Applications are distinct in that we construct an _entire website_ as a single HTML file. We leverage large amounts of DOM manipulation to remove everything from the page and replace it with entirely different contents to appear as if we navigated to a different web page. The benefits are that we bypassed the delay time for a new web page to have loaded.

As we perform this DOM manipulation, we also modify the URL to make it seem as if the web page truly is different. This action is known as "routing". Each new "page" is listed under a different url or "route".

## Components

A "Component" is a small section of a web page. The purpose is to provide re-usability for portions of the webpage.
Each Component controls 3 aspects: The view, the data, and the logic.

Components can represent large or small portions/sections of a web page. It is possible for a Component to represent an entire "page" of a website. Or a Component might represent a small footer of a single webpage (and reused where necessary).

The entire website is then composed of these components. They are nested and interwoven to produce the resulting website.

## Implementation

Angular has several high-level constructs that it uses to organize your entire project.

- Components
- Services
- Modules

There are various smaller pieces as well, but the above 3 are the most central.

### Components

Exactly what was described above.
In your Angular project, you will have 3 (or 4) files for each component:
- `.html` file for the view
- `.css` file for styling
- `.ts` file for the logic
- `.spec.ts` file for testing

Use the @Component decorator above your TypeScript class.
Inside this decorator includes some configuration that binds the html, css, and typescript files together.

Example

```typescript
@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css'],
})
export class NavBarComponent {
    ...

    // Angular will automatically inject the AuthService into the component
    constructor(private authService: AuthService) {}
}
```

### Services

Occasionally your web application will need logic that is re-used throughout the application. This logic may not be directly associated with any view at all. Examples might include sending requests to an API and managing that data. It might include dealing with authorization, etc.

This type of logic is kept in a "Service" in Angular. It is isolated/independent logic for the application that is not associated with any specific view/component.

Services support being injected into your Component classes via Dependency Injection.

Use @Injectable decorator above your TypeScript class.

Example

```typescript
@Injectable({
    providedIn: 'root',
})
export class AuthService {
    ...
}
```

### Modules

Modules in Angular a high-level organizational construct. Modules contain Components and Services.
Modules can import/export Components/Services from and between them.

This is useful because Angular supports some performance optimization features through lazily-loaded modules.

When you go to an Angular website, your computer does not need to load the entire website, if only 1 module might actually be used.
Instead, we only load modules as they are actively used by the user.

Note that this is not supported automatically by Angular. You must specifically design your application to leverage lazily loaded modules.

Uses the @NgModule decorator. Defines which components are included in your module.

Example

```typescript
@NgModule(
    declarations: [
        NavBarComponent
    ],
    imports: [
        BrowserAnimationsModule // Built-in Module
    ],
    bootstrap: [AppComponent], // Only need this property for the root Module
    providers: [] // Advanced Configuration
)
export class AppModule {}
```