# Angular Versioning

Google first released AngularJS in 2010. AngularJS is very similar to Angular, except it is the precurser, so some aspects are less fleshed out, and it uses JavaScript instead of TypeScript (As TypeScript was not released yet).

Note at the time, this framework was just called "Angular".

AngularJS then went through an update, so it was AngularJS 2, or at the time, was known as Angular 2.

However, then TypeScript was released, and the many issues of AngularJS started being revealed, so Google recreated the framework using TypeScript and fixing many of its issues.

This new framework was now called Angular, and the original framework was renamed to AngularJS.

Some people referred to this new framework as Angular 2.
However, this new framework also came out with a second version, so the phrase "Angular 2" had quite a few meanings.

"Angular 2" could refer to:
1. the general second framework
2. the second version of the second framework
3. The second version of AngularJS

The terminology became very vague and ambigious.

Due to this, when doing your own research, by searching Google for solutions in Angular, you might occasionally see posts that are talking about AngularJS. You must be careful about this, because the syntax is slightly different, albeit having a lot of similarities.

Solutions for AngularJS will not work for Angular 2.

A lot of people will search their specific version of Angular, such as "Angular 2+" or "Angular 4" or "Angular 10" to try to be more specific.

# NodeJS

We've previously disucssed that browsers can directly run JS code. Through the use of the html `script` tag.
Over time, people wanted to be able to run JS code separately from the browser.

So that is what NodeJS is. NodeJS is a separate runtime for JS.
In the same way that JS runs on the browser, you would say that JS runs on NodeJS.
The terminology is that NodeJS is a "JS Runtime".

This effectively provides server-side JavaScript.

NodeJS comes with a package manager called `npm`, which stands for `Node Package Manager`.

# NPM

NPM is similar to many other package managers that you might have seen such as Chocolatey, or scoop, or yum or apt-get on linux.

## What is a package manager?

A general software tool that handles the installation, removal, and versioning of other sotware packages.

Typically, package managers come with commands such as `install`, `uninstall` (or perhaps `remove`), `update`, etc.
They also commonly support the ability to search for packages and read information about them.

NPM is a package manager for NodeJS projects. As such, it does not work for browser executed JS.

# Webpack

Weback is known as a JS Module "bundler".

Any module bundler (we typically use webpack), will take all of your NodeJS modules (your various files and dependencies), and wrap them together and produce an html file with various large JS files that are included in the html file via script tags.

Our Angular application is transpiled into JavaScript, then bundled into static assets via webpack, and then hosted on a web-server via `ng serve`.

If we did not want to run this locally, we could use `ng build` to produce the static assets, and we could place them on our own, separate, web-server.