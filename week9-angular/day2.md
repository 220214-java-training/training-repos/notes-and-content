# Application Flow for Angular

Our Angular applications are bootstrapped via a provided `main.ts` file.
This file bootstraps the core module for the application, generally named `AppModule` in file `app.module.ts`.

The `AppModule` imports various other required modules, most notably an `AppRoutingModule` if routing is enabled, which is found in `app-routing.module.ts` file.

The `AppRoutingModule` defines the various routes for the application. This allows our Single Page Application to emulate an entire website. This defines which components should be rendered underneath the `<router-outlet>` element depending on the url.
It is also common to create a wildcard route that will redirect to some default location.

From there, the `AppModule` generally defines one or more components to be bootstrapped. This means these components will be instantiated and processed ahead of time to be rendered from the `index.html` file.
From there, Angular will process any component that is included in the html template files across any currently renedered component.

## Our Angular-Demo Example

In our example, our `AppModule` bootstrapped the `AppComponent`. Our `AppRoutingModule` defined 2 routes: 1 for the `DashboardComponent` and another to redirect to the `DashboardComponent`.

Our `AppComponent` included a `<router-outlet>`, which then rendered our `DashboardComponent`.
The `DashboardComponent` then rendered two copies of the `ClientCardComponent` as part of its template.