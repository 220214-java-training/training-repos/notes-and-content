create table department(
	id serial primary key,
	name varchar(30) not null
);

insert into department (id, name) values ('8329', 'another string');

delete from department 
where id>1;

insert into department (name) values ('grocery');
insert into department (name) values ('electronics');

-- added the department column to the product table 
alter table product add column department integer references department(id);

-- add values to the department column
update product
set department = 1;

-- adding some more product records in another department
insert into product values (6, 'apples', 2.00, 3);
insert into product values (7, 'bread', 3.00, 3);
insert into product values (8, 'cheese', 4.00, 3);
insert into product values (9, 'spatula', 7.50, null);
-- insert into product (id, name, price) values (9, 'spatula', 7.50); -- this is equivalent to the last statement 

select *
from product;

select price 
from department;

select *
from department;

-- using a join to display information from both the product table and the department
select product.name, product.price, department.name
from product
join department 
on product.department = department.id
order by department.name desc;

select product.name, product.price, department.name
from product left join department 
on product.department = department.id;

select product.name, product.price, department.name
from product right join department 
on product.department = department.id;

select product.name, product.price, department.name
from product full join department 
on product.department = department.id;

select product.name, product.price, department.name
from product
join department 
on product.department = department.id
where department.name = 'clothing';

-- creating a column
create view clothing_products as
select 
	product.name product_name, 
	product.price, 
	department.name department_name
from product
join department 
on product.department = department.id
where department.name = 'clothing';

select * from clothing_products;

select * from clothing_products
where price > 10;

insert into product values (2, 'pants', 20.00, 1);

select *
from product 
order by price desc;

create view products_by_price
as
select *
from product 
order by price desc;

select *
from products_by_price;

-- renaming columns so that we dont have naming conflicts
alter table department
rename column name to department_name;

alter table product 
rename column name to product_name;

select product_name, product.price, department_name
from product full join department 
on product.department = department.id;

