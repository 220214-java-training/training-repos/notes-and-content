--- run this script to set up the department/product schema
drop view if exists clothing_products;
drop view if exists products_by_price;
drop table if exists sales_representative;
drop table if exists product;
drop table if exists department;



create table department(
	id serial primary key,
	name varchar(30) not null
);

create table product(
	id integer primary key, -- primary key = unique non null
	name varchar(60) not null,
	price numeric(5,2) check (price>0), -- up to 999.99
	department integer references department(id)
);

insert into department (name) values ('clothing');
insert into department (name) values ('grocery');
insert into department (name) values ('electronics');


insert into product values (1,'shoes',40.00,1);
insert into product values (2,'shirt',17.50,1);
insert into product values (3,'shirt',15.50,1);
insert into product values (4, 'socks', 3.00,1);
insert into product values (5, 'dress',35.00,1);
insert into product values (6, 'apples', 2.00, 3);
insert into product values (7, 'bread', 3.00, 3);
insert into product values (8, 'cheese', 4.00, 3);
insert into product values (9, 'spatula', 7.50, null);