# JDBC

- Java Database Connectivity
- a means for accessing our DB with a Java application
- postgres provides a dependency for doing this with a postgres database -- we can obtain the postgres jdbc driver with Maven
- this will allow us to leverage the functionality of the classes and interfaces in the API

## Connection

- represents a connection, or a session, with the database, which allows us to execute sql statements and retrieve their results
- create using the DriverManager class' static getConnection method, along with our database credentials

## Statement

- used for executing a static SQL statement and returning the results it produces
- execution methods will execute them in the database
- autocommit by default is true
- creating a new statement

```java
Connection con = ConnectionUtil.getConnection();
Statement s = con.createStatement();
ResultSet rs = s.executeQuery("select * from account");
```

## PreparedStatement

- subinterface of statement, but it is compiled separately from the parameters to prevent sql injection

```java
Connection con = DriverManager.getConnection(...);
con.prepareStatement("select * from account where account_id = ?")
// statement.set[ type ]( [1 based index ],[ value ])
statement.setString(1, id)
```

## CallableStatement

- subinterface of prepared statement
- used in order to execute stored procedures or function

```java
Connection con = DriverManager.getConnection(...);
CallableStatement cs = con.prepareCall("{call procedure_name(?,..)}"));
// cs.set[type]([index], [value]);
cs.execute();
```

## ResultSet

- table of data representing a database result set, returned from a statement
- maintains a cursor pointing to its current row of data
- initially the cursor is positioned before the first row
- next method moves the cursor to the next row, returns false when there are no more rows in the ResultSet object

```java
statement.executeQuery()
rs.next();
// rs.get[type]([column index]/[column name])
rs.getString("name");
```

## Transactions in JDBC

- by default, JDBC will auto-commit executed statements
- to create explicit transactions with JDBC, we can turn off auto-commit (use the Connection method "setAutocommit")
- from there, we are able to use provided connection methods to create savepoints, rollback, and commit 
- see an example similar to our actor/movie demo [here](https://www.postgresqltutorial.com/postgresql-jdbc/transaction/)

# DAO Design Pattern

- provides a standard interface for performing data access operations
- promotes loose coupling and allows for interchangeability between data access code


# Protecting Your DB Credentials

When connecting to your database, you'll need to provide a connection string, along with your username and password. We never want to hardcode sensitive information like a username/password pair into your code. There are a few ways we can keep our credentials out of our source code.

1. Properties File 
- properties files allow you to store key value pairs together in a separate file 
- you can include your username and password in a separate file, and then include the properties file in your .gitignore so that it is not tracked by git

In db.properties:
```properties
user=postgres
password=p4ssw0rd
```

In your java code:
```java
        try (InputStream input = new FileInputStream("src/main/resources/db.properties")) {
            
            Properties props = new Properties();
            props.load(input);

            Connection c = DriverManager.getConnection("jdbc:postgresql://host:5432/postgres", props);
        
        } catch(SQLException | IOException e) {
            e.printStackTrace();
        }
```

2. Environment Variables
- another way that we can protect our credentials is by parameterizing credentials using environment variables

In your java code:
```java
        try {
            String username = System.getenv("DB_USER");
            String password = System.getenv("DB_PASS");

            Connection c = DriverManager.getConnection("jdbc:postgresql://host:5432/postgres", username, password);

        } catch (SQLException e) {
            e.printStackTrace();
        }
```

You can then configure environment variables on your local machine, or set them in the run configuration of your application in IntelliJ:

<img src="./note-images/run-config.png" width="700">

<img src="./note-images/env.PNG" width="500">
